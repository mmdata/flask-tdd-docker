import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# instantiate the db (we will link it to the app later)
db = SQLAlchemy()


def create_app(script_info=None):

    # Instantiate the app
    app = Flask(__name__)

    # set config
    app_settings = os.getenv("APP_SETTINGS")
    app.config.from_object(app_settings)

    # set up extensions
    db.init_app(app)

    # register blueprint
    from src.api.ping import ping_blueprint

    app.register_blueprint(ping_blueprint)

    from src.api.users import users_blueprint

    app.register_blueprint(users_blueprint)

    # shell context for flask cli
    # With this we can work with app and db directly in the shell
    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    return app
