# Test-Driven Development with Python, Flask, and Docker

[![pipeline status](https://gitlab.com/mmData/flask-tdd-docker/badges/master/pipeline.svg)](https://gitlab.com/mmData/flask-tdd-docker/commits/master)

# Introduction
This app is based on the course https://testdriven.io/courses/tdd-flask/getting-started/

# To execute commands define in the manage.py do:
- `docker-compose exec api python manage.py name_command`

# Run with docker compose
First we have to build the image:
- `docker-compose build`
Then we fire up the container. We can do that in detached mode(not showing the logs):
- `docker-compose up -d`
Or we can fire it up showing the logs:
- `docker-compose up`
To update the container (rebuild it):
- `docker-compose up -d --build`
To clean previously used container we can execute the commands with
-  `--remove-orphans`
To check that the container is running go to 
- `http://0.0.0.0:5004/ping`

# Run the test
First check that the container is up, then:
- `docker-compose exec api python -m pytest "src/tests"`
To run only specific tests, like only those with the word config in the name:
- `docker-compose exec api python -m pytest "src/tests" -k config`


# Run without Docker
To run the server:
- `export FLASK_APP=src/__init__.py`
- `export FLASK_ENV=development`
- `python manage.py run`

# Run the shell
To run the shell:
- `docker-compose exec api flask shell`

# Heroku Deployment
The API is running at `https://ancient-bastion-46428.herokuapp.com/ping/`

# CI/CD
Check that the user of the repository must be lowercase, so mmdata, not mmData
- `heroku auth:token` gives the token from the Heroku.
- In Gitlab set a variable called HEROKU_AUTH_TOKEN with this value